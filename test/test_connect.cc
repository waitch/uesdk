#include "uesdk/uesdk.h"
#include "uesdk/cJSON.h"
#include <iostream>

#define CURL_STATICLIB

static uesdk::http::HttpConnectionPool::ptr s_qtch_conn_ptr = uesdk::http::HttpConnectionPool::Create("https://qtch.waitch.xyz","https://qtch.waitch.xyz",10,500*1000,10);

void test_login(){
    std::cout << "test_login" << std::endl;
    uesdk::http::HttpResult::ptr login_res = s_qtch_conn_ptr->DoGet("https://qtch.waitch.xyz/user/login/pwd?userName=test3&passWord=123456789",5000);
    if(login_res->m_result != uesdk::http::HttpResult::Error::OK){
        std::cout << "login faile"
        << " errstr=" << login_res->m_error
        << std::endl;
        return;
    }
    std::cout << "login success:\n"
        << login_res->m_resonse << std::endl;
}

void test_mutil_http(){
    std::cout << "test_mutil_http" << std::endl;
    uesdk::http::HttpResult::ptr login_res = s_qtch_conn_ptr->DoGet("https://qtch.waitch.xyz/user/login/pwd?userName=test3&passWord=123456789",5000);
    if(login_res->m_result != uesdk::http::HttpResult::Error::OK){
        std::cout << "login faile"
        << " errstr=" << login_res->m_error
        << std::endl;
        return;
    }
    std::cout << "login success:\n"
        << login_res->m_resonse << std::endl;
    cJSON* login_cjson = cJSON_Parse(login_res->m_resonse->getBody().c_str());
    cJSON* code = cJSON_GetObjectItem(login_cjson,"code");
    if(!code|| strcmp("0",code->valuestring)!=0){
        std::cout << "login error response:[\n" << login_res->m_resonse << "]" << std::endl;
        return; 
    }
    cJSON* data = cJSON_GetObjectItem(login_cjson,"data");
    cJSON* c_token = cJSON_GetObjectItem(data,"token");
    std::string token = c_token->valuestring;
    std::cout << "token:" << token << std::endl;
    std::map<std::string,std::string> header;
    header["token"] = token;
    header["connection"] = "keep-alive";
    for(int i=0;i<10;i++){
        uesdk::http::HttpResult::ptr get_res = s_qtch_conn_ptr->DoGet("https://qtch.waitch.xyz/user/get?id=95",5000,header);
        if(get_res->m_result != uesdk::http::HttpResult::Error::OK){
            std::cout << "get_res faile"
            << " errstr=" << get_res->m_error
            << std::endl;
            return;
        }
        std::cout << "get " << i << " response:[\n" << get_res->m_resonse << "]" << std::endl;
    }
}

int main(){
    std::cout << "begin" << std::endl;
    test_mutil_http();
}