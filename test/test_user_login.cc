#include "uesdk/user_manger.h"


int test_user_login(){
    uesdk::User::ptr user = uesdk::UserMgr::GetInstance()->login("test3","123456789");
    if(!user){
        std::cout << "login faile" << std::endl;
    }
    std::string token = user->getData<std::string>("token");
    std::string flashToken = user->getData<std::string>("flashToken");
    std::string userName = user->getData<std::string>("userName");
    std::string password = user->getData<std::string>("password");
    uint64_t tokenExpire = user->getData<uint64_t>("tokenExpire");
    uint32_t id = user->getId();
    uint64_t now = time(NULL);
    std::cout << "id=" << id
        << " token=" << token
        << " flashToken=" << flashToken
        << " userName=" << userName
        << " password=" << password
        << " tokenExpire=" << tokenExpire
        << " now=" << now << std::endl;
    return 1;
}

int main(){
    test_user_login();
}