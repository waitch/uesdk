#ifndef __UESDK_MUTEX_H__
#define __UESDK_MUTEX_H__

#include "uesdk/noncopyable.h"
#include <boost/thread/mutex.hpp>

namespace uesdk{

template<class T>
class ScopedLockImp{
public:
    ScopedLockImp(T& mutex)
    :m_mutex(mutex){
		m_locked = false;
        lock();
    }
    ~ScopedLockImp(){
        unlock();
    }
    
    void lock(){
        if(!m_locked){
            m_mutex.lock();
            m_locked = true;
        }
    }
    
    void unlock(){
        if(m_locked){
            m_mutex.unlock();
            m_locked = false;
        }
    }

private:
    T& m_mutex;
    bool m_locked;
};

class Mutex : Noncopyable{
public:
    typedef ScopedLockImp<Mutex> Lock;
    Mutex(){
    }

    ~Mutex(){
    }

    void lock(){
        m_mutex.lock();
    }

    void unlock(){
        m_mutex.unlock();
    }


private:
    boost::mutex m_mutex;
};
    
} // namespace uesdk


#endif