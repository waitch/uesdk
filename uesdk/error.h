#ifndef __UESDK_ERROR_H__
#define __UESDK_ERROR_H__

#include <string>
#include "uesdk/singleton.h"
namespace uesdk{

enum UesdkErrorCode{
    UESDK_ERROR_DEFAULT = -1,
    UESDK_ERROR_OK = 0,
    UESDK_ERROR_REPEAT_LOGIN = 1,
    UESDK_ERROR_LOGIN_FAILe,
    UESDK_ERROR_NEED_LOGIN,

    UESDK_ERROR_PARAM_ERROR = 100,


    UESDK_ERROR_REQUEST_ERROR = 200,

    UESDK_ERROR_THIRD_CHECKTOMANYTIMES = 300,
};
    
class Error{
public:
    void setError(UesdkErrorCode error_id,const std::string& errorMsg) {
        m_error_id = error_id;
        m_error_message = errorMsg;
    }
    UesdkErrorCode getErrorId() const {return m_error_id;}
    const std::string& getErrorMsg() const {return m_error_message;} 

private:
    UesdkErrorCode m_error_id;
    std::string m_error_message;
};

typedef Singleton<Error> ErrorMgr;


} // namespace uesdk

#endif