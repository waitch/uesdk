#ifndef __UESDK_NONCOPYABLE_H__
#define __UESDK_NONCOPYABLE_H__

namespace uesdk{


class Noncopyable{
public:
    Noncopyable(){}
    ~Noncopyable(){}
private:
    Noncopyable(const Noncopyable& copy);
    Noncopyable operator = (const Noncopyable& copy);
};

}




#endif