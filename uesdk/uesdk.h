#ifndef __UESDK_UESDK_H__
#define __UESDK_UESDK_H__

#include <stdlib.h>
#include <stdint.h>
#include "config.h"



#ifdef __cplusplus
extern "C" {
#endif


typedef struct{
    int32_t id;
}UeUser;

typedef struct{
    int third_type;
    char * authorUrl;
    char * code;
    char * verifyCode;
    int check_count;
}UeThirdStatus;


UESDK_DLL_API void UeUserInit(UeUser* user);

/**
 * @brief login by userName and password
 *
 * @param user
 * @param user_name userName
 * @param password password
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeUserLogin(UeUser* user,char* user_name,char* password);

/**
 * @brief login by Tel and verfiy code
 * 
 * @param user 
 * @param tel 
 * @param code 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeUserLoginByTel(UeUser* user,char* tel,char* code);

/**
 * @brief login by falshToken
 * 
 * @param user 
 * @param flashToken 
 * @param flashTokenSize 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeUserLoginByFlashToken(UeUser* user,char* flashToken,size_t flashTokenSize);

typedef void (*ThirdLoginFailedCallback)(UeUser* user,void *data);
typedef void (*ThirdLoginSuccessCallback)(UeUser* user,int32_t id,void *data);

/**
 * @brief Third party platform login
 *
 * @param user
 * @param type  Third party platform login such as UETHIRDLOGINQQ，UETHIRDLOGINWECHAT
 * @param successCallback Callback executed after success
 * @param faileCallback Callback executed after failure
 * @param data Data used during callback
 * @param wait_sec Wait for third-party confirmation time. By default, wait for 5 minutes. After the wait time has passed, fail Callback will be called
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeUserLoginThird(UeUser* user,int32_t type,ThirdLoginSuccessCallback successCallback, ThirdLoginFailedCallback faileCallback,void* data, int64_t wait_sec);

/**
 * @brief get token
 *
 * @param user
 * @param token_out[out]
 * @param size [out]
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeGetUserToken(UeUser* user,char** token_out,int* size);


/**
 * @brief get flashtoken
 * 
 * @param user 
 * @param flashtoken_out [out]
 * @param size [out]
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeGetFlashUserToken(UeUser* user,char** flashtoken_out,int* size);

/**
 * @brief is logined
 * 
 * @param user 
 * @return int login 1, unlogin 0
 */
UESDK_DLL_API int UeisUserlogined(UeUser* user);

UESDK_DLL_API int UeGetLastError();
UESDK_DLL_API const char* UeGetLastErrorMsg();

/**
 * @brief Send mobile login verification code
 * 
 * @param phone Send mobile login verification code
 * @param phone_size 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UePhoneLoginVerify(char * phone,size_t phone_size);

/**
 * @brief Send mobile registration verification code
 * 
 * @param phone 
 * @param phone_size 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UePhoneRegisterVerify(char * phone,size_t phone_size);

/**
 * @brief Send email register verification code
 * 
 * @param email 
 * @param email_size 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeEmailRegisterVerify(char * email,size_t email_size);

/**
 * @brief Send email login verification code
 * 
 * @param email 
 * @param email_size 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeEmailLoginVerify(char * email,size_t email_size);


/**
 * @brief Obtaining stored user data
 * 
 * @param user 
 * @param key 
 * @param key_len 
 * @param data [out] data
 * @param data_len [out] 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeGetUserDataString(UeUser* user,char* key,size_t key_len, char** data,size_t* data_len);


/**
 * @brief Third party QQ login request
 * 
 * @return UeThirdStatus* 
 */
UESDK_DLL_API UeThirdStatus* UeCreateThirdLoginQQ();

/**
 * @brief Third party WeChat login request
 * 
 * @return UeThirdStatus* 
 */
UESDK_DLL_API UeThirdStatus* UeCreateThirdLoginWechat();

/**
 * @brief Detect if third-party login succeeded
 * 
 * @param user 
 * @param thirdStatus 
 * @return int int logined 1, not login -1, error 0
 */
UESDK_DLL_API int UeThirdLoginCheck(UeUser* user,UeThirdStatus* thirdStatus);

/**
 * @brief Register through mobile verification code
 * 
 * @param user_name 
 * @param password 
 * @param tel 
 * @param verifyCode 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeRegisterUserByTel(char * user_name, char* password, char* tel, char* verifyCode);

/**
 * @brief Register through email verification code
 * 
 * @param user_name 
 * @param password 
 * @param email 
 * @param verifyCode 
 * @return int success 1,error 0
 */
UESDK_DLL_API int UeRegisterUserByEmail(char * user_name, char* password, char* email, char* verifyCode);





/**
 * @brief Determine if it is a phone number
 * 
 * @param phone 
 * @param phone_size 
 * @return int phone 1,error 0
 */
UESDK_DLL_API int UeisPhoneNumber(char * phone,size_t phone_size);

/**
 * @brief Determine if it is an email
 * 
 * @param email 邮箱
 * @param email_size email长度
 * @return int email 1,error 0
 */
UESDK_DLL_API int UeisEmail(char * email,size_t email_size);

#ifdef __cplusplus
}
#endif



#endif
