#ifndef __UESDK_CONFIG_H__
#define __UESDK_CONFIG_H__


#ifdef UESDK_DLL_EXPORT
#define UESDK_DLL_API _declspec(dllexport)
#else
#define UESDK_DLL_API 
#endif

#define UESDK_SERVER_URL "https://im.uesoft.com:8020"
#define UESDK_SERVER_HOST "im.uesoft.com"



#endif