#ifndef __UESDK_SINGLETON_H__
#define __UESDK_SINGLETON_H__

#include <boost/smart_ptr.hpp>

namespace uesdk{


template<class T>
class Singleton {
public:
    static T* GetInstance(){
        static T v;
        return &v;
    }
};

template<class T>
class SingletonPtr{
public:
static boost::shared_ptr<T> GetInstance(){
    static boost::shared_ptr<T> v(new T);
    return v;
}
};


}


#endif