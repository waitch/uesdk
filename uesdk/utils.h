#ifndef __UESDK_UTIL_H__
#define __UESDK_UTIL_H__

#include <string>
#include <vector>
#include <stdint.h>
#include "uesdk/config.h"

namespace uesdk{
    
uint64_t GetCurrentMS();

std::string Time2Str(time_t ts, const std::string& format);
std::string Gmtime2Str(time_t ts, const std::string& format);

class UESDK_DLL_API StringUtil{
public:
    static std::string trim(const std::string& str,const std::string& delimit = " \t\r\n");
    static std::vector<std::string> split(const std::string& str, char delim, size_t max = ~0);
    static std::vector<std::string> split(const std::string& str, const char* delim, size_t max = ~0);
    static std::string replace(const std::string& src, char find, char replaceWitch);
    static std::string replace(const std::string& src, char find, const std::string& replaceWitch);
    static std::string toLower(const std::string& v);
    static std::string toUpper(const std::string& v);
    static std::vector<std::string> regexSearch(const std::string& src,const std::string& pattern);
    static bool regexMatch(const std::string& src,const std::string& pattern);
    static std::string regexReplace(const std::string& src, const std::string& pattern, const std::string& replaceWitch);
};

class UESDK_DLL_API TypeUtil{
public:
    static bool isPhoneNumber(const std::string& phone);
    static bool isEmail(const std::string& email);
};

    
} // namespace uesdk


#endif